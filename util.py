'''
Created on Nov 18, 2013

@author: yonatanf
'''
import numpy as np
import scipy.stats as stats
from numpy.random import RandomState

def likelihhodRatioTest(ll0,ll1,df0,df1):
    D = 2*(ll1-ll0) # test statistic
    ddf = df1 - df0 # difference in degrees-of-freedom
    p = stats.chi2.sf(D,ddf)
    return p    

def logMeanExp(x, axis=0):
        xmax = x.max(axis)
        dx   = x - xmax
        x_normed = np.exp(dx) # = x/x_max
        return xmax + np.log(x_normed.mean(axis)) 

def sim_counts(N,f,e, seed=None):
    prng = RandomState(seed)
    
    n,k = np.shape(f)
    fe = f*(1-e)+(1-f)*e/(k-1)
    counts = np.zeros((n,k))
    for i,(Ni,fi) in enumerate(zip(N,fe)):
        counts[i,:] = prng.multinomial(Ni,fi)
    return counts.astype(int)

def filter_counts(counts, c_min=-1):
    x = counts.copy()
    x.sort(axis=1)
    x = x[:,-2:]
    return x[x[:,0]>c_min,:]
    
def get_mono_pval(counts,e=0.01):
    N = counts.sum()
    m = counts.max()
    return 1-stats.binom(N,e).cdf(N-m-1)

def get_poly_sites(counts, e=0.01, p_th=0.01):
    p = np.apply_along_axis(get_mono_pval, 1, counts, e)
    inds = (p<p_th).nonzero()
    return counts[inds], inds

if __name__ == '__main__':
    pass